<!-- JSPの共通ヘッダー -->

<%@page contentType="text/html" pageEncoding="UTF-8" session="false"%>
<head>
<title>tool-taro.com</title>
<meta charset="utf-8">

<link rel="stylesheet" href="./css/CommonHeader.css">
</head>
<body>
<div class="header">
  <div class="header__inner">
      <img class="header__logo" src="./picture/logo.png?text=LOGO">
    <div class="header__navgroup">
      <div class="header__navitem">TOP</div>
      <div class="header__navitem">ABOUT</div>
      <div class="header__navitem">BLOG</div>
      <div class="header__navitem">CONTACT</div>
    </div>
  </div>
</div>
</body>
