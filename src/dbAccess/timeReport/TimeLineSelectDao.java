package dao.timeline;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.Timeline;
import util.UtilConstant;

public class TimeLineSelectDao {

	UtilConstant utilConstant = new UtilConstant();
	Connection connection = null;
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;

	public void GetAttendance(Integer id, Date firstDay, Date lastDay) {

		try {
			Class.forName(utilConstant.DB_DRIVER);
			connection = DriverManager.getConnection(utilConstant.DB_URL, utilConstant.DB_USER,
					utilConstant.DB_PASSWORD);

			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("SELECT * FROM timeline where staff_id = ? ");
			stringBuilder.append("and datetime between ? and ?");
			String sql = new String(stringBuilder);

			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, id);
			preparedStatement.setDate(2, firstDay);
			preparedStatement.setDate(3, lastDay);

			resultSet = preparedStatement.executeQuery();

			List<Timeline> timeLineList = new ArrayList<>();

			while (resultSet.next()) {
				resultSet.getDate("datetime");
				resultSet.getTimestamp("coming_time");
				resultSet.getTimestamp("leaving_time");
				System.out.println(resultSet.getDate("datetime"));
				System.out.println(resultSet.getTimestamp("coming_time"));
				System.out.println(resultSet.getTimestamp("leaving_time"));
				resultSet.getTimestamp("break_time");
				resultSet.getTimestamp("over_time");
				resultSet.getTimestamp("actual_time");
				resultSet.getString("location");
				resultSet.getString("content");
				resultSet.getString("memo");

				Timeline timeLine = new Timeline(id, lastDay, null, null, null, null, null, sql, sql, sql);

			}

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			//		select * from timeline where staff_id = 1 and datetime between '2020/11/1' and '2020/11/30';
		}
	}
}
