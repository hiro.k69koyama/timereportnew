create table public.mst_date_type (
  date_type_id varchar(10) not null
  , date_type_name varchar(10) not null
  , regist_date date
  , update_date date
  , primary key (date_type_id)
);