create table public.mst_user_info (
  user_id varchar(10) not null
  , user_name varchar(50) not null
  , mail_address varchar(100) not null
  , password varchar(20) not null
  , department_id varchar(10) not null
  , authority_id varchar(10) not null
  , regist_date date
  , update_date date
  , primary key (user_id)
);
