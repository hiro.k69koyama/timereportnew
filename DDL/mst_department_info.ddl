create table public.mst_department_info (
  department_id varchar(10) not null
  , department_name character varying(20) not null
  , regist_date date
  , update_date date
  , primary key (department_id)
);