create table public.work_time (
  user_id varchar(10) not null
  , work_time time not null
  , regist_date date
  , update_date date
  , primary key (user_id)
);