create table public.mst_authority (
  authority_id varchar(10) not null
  , authority_name character varying(20) not null
  , regist_date date
  , update_date date
  , primary key (authority_id)
);