create table public.mst_project (
  project_id varchar(10) not null
  , project_name varchar(10) not null
  , regist_date date
  , update_date date
  , primary key (project_id)
);