create table public.time_report (
  time_report_id varchar(10) not null
  , user_id varchar(10) not null
  , working_date date not null
  , date_type_id varchar(10) -- 日程種別
  , start_time time -- 開始時刻
  , finish_time time -- 終了時刻
  , break_time time -- 休憩時間
  , over_time time -- 残業時間
  , actual_time time -- 業務時間
  , project_01_id varchar(10) -- プロジェクト01ID
  , project_01_actual_time time -- プロジェクト01業務時間
  , project_02_id varchar(10) -- プロジェクト02ID
  , project_02_actual_time time -- プロジェクト02業務時間
  , project_03_id varchar(10) -- プロジェクト03ID
  , project_03_actual_time time -- プロジェクト02業務時間
  , location varchar(20) -- 業務場所
  , content character varying(50) -- 業務内容
  , memo character varying(200) -- 備考
  , regist_date date
  , update_date date
  , primary key (time_report_id)
);